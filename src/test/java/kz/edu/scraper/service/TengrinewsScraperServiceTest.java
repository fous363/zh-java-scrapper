package kz.edu.scraper.service;

import kz.edu.scraper.repository.NewsRepository;
import kz.edu.scraper.service.mapper.TengrinewsMapper;
import kz.edu.scraper.service.scraper.TengrinewsScraperService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TengrinewsScraperServiceTest {

    @Mock
    private NewsRepository newsRepository;

    @Spy
    private TengrinewsMapper newsMapper;

    @InjectMocks
    private TengrinewsScraperService scraperService;

    @Test
    void testScrape() {
        when(newsRepository.existsByResourceUrl(anyString())).thenReturn(true);

        scraperService.scrape();

        verify(newsRepository).saveAll(any());
    }
}
