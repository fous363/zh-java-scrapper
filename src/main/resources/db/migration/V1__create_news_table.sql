CREATE TABLE IF NOT EXISTS news (
    id BIGSERIAL PRIMARY KEY,
    title VARCHAR NOT NULL,
    posted_at TIMESTAMP NOT NULL,
    content TEXT NOT NULL,
    resource_name VARCHAR NOT NULL,
    resource_url VARCHAR NOT NULL UNIQUE
);