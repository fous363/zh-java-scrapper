package kz.edu.scraper.controller;

import kz.edu.scraper.entity.News;
import kz.edu.scraper.repository.NewsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/api/news")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ApplicationController {

    private final NewsRepository newsRepository;

    @GetMapping
    public List<News> getNews(@RequestParam(name = "title", required = false) String title,
                              @RequestParam(name = "resource_name", required = false) String resourceName,
                              @RequestParam(name = "page_size", defaultValue = "20") Integer pageSize) {
        if (Objects.nonNull(title) && !title.isBlank()) {
            return newsRepository.findByTitleContainingIgnoreCase(title);
        } else if (Objects.nonNull(resourceName) && !resourceName.isBlank()) {
            return newsRepository.findAllByResourceName(resourceName, Pageable.ofSize(pageSize));
        }
        return newsRepository.findAll(Pageable.ofSize(pageSize)).getContent();
    }
}
