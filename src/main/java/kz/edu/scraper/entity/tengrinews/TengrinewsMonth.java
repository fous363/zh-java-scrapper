package kz.edu.scraper.entity.tengrinews;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.time.Month;
import java.util.Arrays;

@RequiredArgsConstructor
@Getter
public enum TengrinewsMonth {
    SEPTEMBER(Month.SEPTEMBER, "сентября"),
    OCTOBER(Month.OCTOBER, "октября"),
    NOVEMBER(Month.NOVEMBER, "ноября"),
    DECEMBER(Month.DECEMBER, "декабря"),
    JANUARY(Month.JANUARY, "января"),
    FEBRUARY(Month.FEBRUARY, "февраля"),
    MARCH(Month.MARCH, "марта"),
    APRIL(Month.APRIL, "апреля"),
    MAY(Month.MAY, "мая"),
    JUNE(Month.JUNE, "июня"),
    JULY(Month.JULY, "июля"),
    AUGUST(Month.AUGUST, "августа");

    private final Month javaMonth;
    private final String articleMonth;

    public static Month getMonth(String value) {
        return Arrays.stream(TengrinewsMonth.values())
                .filter(tengrinewsMonth -> tengrinewsMonth.getArticleMonth().equalsIgnoreCase(value))
                .findFirst()
                .map(TengrinewsMonth::getJavaMonth)
                .orElseThrow();
    }
}
