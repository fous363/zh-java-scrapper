package kz.edu.scraper.entity;

import jakarta.persistence.*;
import lombok.*;

import java.time.OffsetDateTime;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Table(name = "news")
public class News {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = "title")
    String title;

    @Column(name = "posted_at")
    OffsetDateTime postedAt;

    @Column(name = "content")
    String content;

    @Column(name = "resource_name")
    String resourceName;

    @Column(name = "resource_url")
    String resourceUrl;
}
