package kz.edu.scraper.repository;

import kz.edu.scraper.entity.News;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface NewsRepository extends PagingAndSortingRepository<News, Long>, CrudRepository<News, Long> {
    List<News> findByTitleContainingIgnoreCase(String title);
    List<News> findAllByResourceName(String resourceName, Pageable pageable);
    @Query("SELECT CASE WHEN COUNT(n) > 0 THEN true ELSE false END FROM News n Where n.resourceUrl = :newsResourceUrl")
    boolean existsByResourceUrl(@Param("newsResourceUrl") String newsResourceUrl);
}
