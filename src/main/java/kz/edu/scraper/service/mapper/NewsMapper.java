package kz.edu.scraper.service.mapper;

import kz.edu.scraper.entity.News;

public interface NewsMapper {
    News map(String articleTitle, String postDate, String content, String articleLink);
}
