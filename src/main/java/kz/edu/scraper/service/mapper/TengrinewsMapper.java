package kz.edu.scraper.service.mapper;

import kz.edu.scraper.entity.News;
import kz.edu.scraper.entity.tengrinews.TengrinewsMonth;
import kz.edu.scraper.exception.MapperException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.*;
import java.util.Arrays;

@Component
@Slf4j
public class TengrinewsMapper implements NewsMapper {

    @Override
    public News map(String articleTitle, String postDate, String content, String articleLink) {
        return News.builder()
                .title(articleTitle.trim())
                .postedAt(mapPostDateToOffsetTimeDate(postDate))
                .content(content.trim())
                .resourceName("Tengrinews")
                .resourceUrl(articleLink.trim())
                .build();
    }

    private OffsetDateTime mapPostDateToOffsetTimeDate(String postDate) {
        String normalizedPostDate = postDate.trim().replace(",", "");
        String[] postDateTokens = Arrays.stream(normalizedPostDate.split(" "))
                .map(String::trim)
                .toArray(String[]::new);
        LocalDate date;
        LocalTime time;
        if (postDateTokens.length == 2) {
            date = mapLiteralDateToLocalDate(postDateTokens[0]);
            time = LocalTime.parse(postDateTokens[1]);
        } else if (postDateTokens.length == 4) {
            int year = Integer.parseInt(postDateTokens[2]);
            Month month = TengrinewsMonth.getMonth(postDateTokens[1]);
            int day = Integer.parseInt(postDateTokens[0]);
            date = LocalDate.of(year, month, day);
            time = LocalTime.parse(postDateTokens[3]);
        } else {
            log.error("[Tengrinews] Cannot map post date of {}", postDate);
            throw new MapperException("Cannot map post date of tengrinews");
        }
        return OffsetDateTime.of(date, time, ZoneOffset.UTC);
    }

    private LocalDate mapLiteralDateToLocalDate(String date) {
        LocalDate now = LocalDate.now();
        if ("Вчера".equalsIgnoreCase(date)) {
            return now.minusDays(1);
        } else if ("Сегодня".equalsIgnoreCase(date)) {
            return now;
        } else {
            log.error("[Tegnrinews] Unknown literal date of {}", date);
            throw new MapperException("Unknown literal date");
        }
    }
}
