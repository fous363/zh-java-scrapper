package kz.edu.scraper.exception;

public class ScraperException extends RuntimeException {

    public ScraperException(String message, Throwable cause) {
        super(message, cause);
    }
}
