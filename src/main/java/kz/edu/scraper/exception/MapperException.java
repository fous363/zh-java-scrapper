package kz.edu.scraper.exception;

public class MapperException extends RuntimeException {
    public MapperException(String message) {
        super(message);
    }
}
