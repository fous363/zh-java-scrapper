package kz.edu.scraper.configuration;

import kz.edu.scraper.service.scraper.NewsScraperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.CronTask;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Configuration
@EnableScheduling
public class ScraperScheduledConf implements SchedulingConfigurer {

    private final String tengrinewsScraperCronExpression;

    private final NewsScraperService tengrinewsScraperService;

    @Autowired
    public ScraperScheduledConf(@Value("${scrapers.tengrinews.scheduled.cron}") String tengrinewsScraperCronExpression,
                                NewsScraperService tengrinewsScraperService) {
        this.tengrinewsScraperCronExpression = tengrinewsScraperCronExpression;
        this.tengrinewsScraperService = tengrinewsScraperService;
    }

    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        ExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        taskRegistrar.setScheduler(scheduledExecutorService);
        CronTask tengrinewsScrapeTask = new CronTask(tengrinewsScraperService::scrape, tengrinewsScraperCronExpression);
        taskRegistrar.addCronTask(tengrinewsScrapeTask);
    }
}
