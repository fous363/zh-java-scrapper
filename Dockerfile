FROM maven:3.8.7-openjdk-18-slim AS builder

ADD ./pom.xml pom.xml

ADD ./src src/

RUN mvn clean package

FROM openjdk:21-slim-bullseye

COPY --from=builder target/*.jar app.jar
COPY ./src/main/resources/application.yaml /app/application.yaml

ARG POSTGRESQL_URL=10.0.1.4/demo_db
ARG POSTGRESQL_USER=db_admin
ARG POSTGRESQL_PASSWORD=superSecre7Passw0rd!

ENV POSTGRESQL_URL=${POSTGRESQL_URL}
ENV POSTGRESQL_USER=${POSTGRESQL_USER}
ENV POSTGRESQL_PASSWORD=${POSTGRESQL_PASSWORD}

CMD ["java", "-jar", "app.jar"]
